<?php 
include_once 'connection.php';

$id = 1001;

$features = array();

$query = mysqli_query($con, "SELECT * from tbl_location where TrackerId = '$id'");
while($row = mysqli_fetch_array($query)){
    
    $loc = explode(",", $row['Location']);
    $lat = floatval($loc[0]);
    $lang = floatval($loc[1]);

    $date = date("m-d-Y h:i:s", strtotime($row['Date']));
    
    $row_arr = array(
        'type' => 'Feature',
        'geometry' => [
                        'type' =>'Point',
                        'coordinates' => [ $lang , $lat ]
                      ],
        'properties' => [
                            'title' => $row['TrackerID'],
                            'description'   =>  "Date: ".$date."<br>"."Location: ".$row['Location'],
                            'icon'  =>  'circle'
                        ]
        );
        
        array_push($features, $row_arr);
}

$response = array();
$response['type'] = "FeatureCollection";
$response['features'] = $features;

echo json_encode($response);
?>