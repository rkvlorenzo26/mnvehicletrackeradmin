<?php 
include_once 'connection.php';

if(isset($_GET['dev_id'])){
    $id = $_GET['dev_id'];
    $from = $_GET['from'];
    $to = $_GET['to'];


	$features = array();

	$query = mysqli_query($con, "SELECT * from tbl_location where TrackerID = '$id' AND Date = '$from'");

	while($row = mysqli_fetch_array($query)){
	    
	    $loc = explode(",", $row['Location']);
	    $lat = floatval($loc[0]);
	    $lang = floatval($loc[1]);

	    $coor = [ $lang , $lat ];
	    
	        
	    array_push($features, $coor);
	}

	$response = array();
	$response['type'] = "Feature";
	$response['properties'] = array();
	$response['geometry'] = array(
	    'type' => "LineString",
	    'coordinates' => $features
	);

	echo json_encode($response);
}
?>