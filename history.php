<?php 
include_once 'connection.php';
session_start();
if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MielNat | Vehicle Tracker</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="icon" href="dist/navigation.png">

  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.css' rel='stylesheet' />
  <style>
     #map { top:0; bottom:0; width:100%; height: 650px; }
  </style>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/navigation.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">MN Vehicle Tracker</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="index.php" class="nav-link">
              <i class="nav-icon fa fa-crosshairs"></i>
              <p>
                Devices
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="history.php" class="nav-link  active">
              <i class="nav-icon fa fa-history"></i>
              <p>
                History
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="geofence.php" class="nav-link">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                Geofence
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="sms.php" class="nav-link">
              <i class="nav-icon fa fa-mobile"></i>
              <p>
                Emergency No.
              </p>
            </a>
          </li>   

          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">History</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-8">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-body">
                <div id='map'></div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>

          <section class="col-lg-4">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">History form</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <label>Start:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                    <input name="from" id="from" type="datetime-local" class="form-control">
                  </div>
                  <!-- /.input group -->
                </div>
                <div class="form-group">
                  <label>End:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                    <input name="to" id="to" type="datetime-local" class="form-control">
                  </div>
                  <!-- /.input group -->
                </div>
                <div class="form-group">
                  <label>Device Name:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-crosshairs"></i>
                      </span>
                    </div>
                    <select id="dev_name" name="dev_name" class="form-control">
                      <option value="">Select device</option>
                      <?php
                        $get_dev = mysqli_query($con, "Select * from tbl_device where UserID = '$_SESSION[user_id]'");
                        while($row = mysqli_fetch_array($get_dev)){
                      ?>
                          <option value="<?php echo $row['TrackerID'] ?>"><?php echo $row['Name']; ?></option>    
                      <?php
                        }
                      ?>
                                      
                    </select>
                  </div>
                  <!-- /.input group -->
                </div>
                <center><button id="btn" onclick="getData()" class="btn btn-primary btn-flat">Show History</button>
                  <a href="javascript:window.location.href=window.location.href" class="btn btn-primary btn-flat">Reset Fields</a>
                </center>
                <br><br>
                <strong>Result guide: </strong><br>
                <i style="color: blue;">*Blue line = Normal Speed</i><br>
                <i style="color: red">*Red line = Overspeed</i>
                <div id="traveled">
                </div>
              </div>
            </div>
          </section>


          <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="index.php">MN Vehicle Tracker</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="jquery-1.10.2.js"></script>

<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoicmt2bG9yZW56bzI2IiwiYSI6ImNpcjk4d3ZmNDAxMXRnOG5rNjVwNzg0ZjMifQ.T2PZrGi-KrZ5u33qj3tlqQ';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [120.9589699, 14.2990183],
    zoom: 8
});
//120.9499528, 14.3021645

// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

function getData(){

  var from = document.getElementById("from").value;  
  var to = document.getElementById("to").value;
  var dev = document.getElementById("dev_name").value;
  var btn = document.getElementById("btn");

  console.log(from+"\n"+to);

  if(from == "" || dev == "" || to == ""){
    alert("Please fill-up all fields.");
  }else{

    document.getElementById("from").disabled = true;
    document.getElementById("to").disabled = true;
    document.getElementById("dev_name").disabled = true;
    document.getElementById("btn").disabled = true;

    //var url = 'get_history.php?dev_id='+dev+'&from='+from+'&to='+to;

    var url = "get_history(line).php?dev_id="+dev+"&from="+from+"&to="+to;
    console.log(url);

    map.addSource('drone', { type: 'geojson', data: url });

    map.addLayer({
        "id": "route",
        "type": "line",
        "source": "drone",
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            'line-width': 4,
            // Use a get expression (https://www.mapbox.com/mapbox-gl-js/style-spec/#expressions-get)
            // to set the line-color to a feature property value.
            'line-color': ['get', 'color']
        }        
      });


    $.post("get_current_history.php",
    {
        dev_id: ""+dev,
        from: ""+from,
        to: ""+to
    },
    function(data){
        var loc = data.split(",");

        map.flyTo({
            center: [loc[0],loc[1]],
            zoom: 16
                
        });
    });

    $.post("get_history_traveled.php",
    {
        dev_id: ""+dev,
        from: ""+from,
        to: ""+to
    },
    function(data){
      $("#traveled").html(data);
    });
  }
}
</script>


</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
