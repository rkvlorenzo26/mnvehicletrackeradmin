<?php 
include_once 'connection.php';

if(isset($_GET['dev_id'])){
    $id = $_GET['dev_id'];
    $from = $_GET['from'];
    $to = $_GET['to'];

    $features = array();

    $query = mysqli_query($con, "SELECT * from tbl_location where TrackerID = '$id' AND Date >= '$from' AND Date <= '$to'");
    while($row = mysqli_fetch_array($query)){

        $name = mysqli_query($con,"SELECT * from tbl_device where TrackerID = '$id'");
        $get_name = mysqli_fetch_array($name);
        
        $loc = explode(",", $row['Location']);
        $lat = floatval($loc[0]);
        $lang = floatval($loc[1]);

        $date = date("m-d-Y h:i:s", strtotime($row['Date']));
        
        $row_arr = array(
            'type' => 'Feature',
            'geometry' => [
                            'type' =>'Point',
                            'coordinates' => [ $lang , $lat ]
                          ],
            'properties' => [
                                'title' => $get_name['Name'],
                                'description'   =>  "Date: ".$date."<br>"."Location: ".$row['Location'],
                                'icon'  =>  'circle'
                            ]
            );
            
            array_push($features, $row_arr);
    }

    $response = array();
    $response['type'] = "FeatureCollection";
    $response['features'] = $features;

    echo json_encode($response);
}
?>