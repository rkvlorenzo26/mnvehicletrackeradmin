<?php 
include_once 'connection.php';

if(isset($_GET['dev_id'])){
   $id = $_GET['dev_id'];
    $from = $_GET['from'];
    $to = $_GET['to'];

    $features = array();

    $a = date("Y-m-d H:i:s", strtotime($from));
    $b = date("Y-m-d H:i:s", strtotime($to));

    $query = mysqli_query($con, "SELECT * FROM `tbl_location` WHERE TrackerID = '$id' AND Date>='$a' AND Date<='$b'");

    $count = 1;
    while($row = mysqli_fetch_array($query)){
		if($count > 1){

		    $loc = explode(",", $row['Location']);
		    $lat = floatval($loc[0]);
		    $lang = floatval($loc[1]);

		    $unit = "K";
		    $distance = distance($lastlat, $lastlang, $lat, $lang, $unit);

		    $lastdate = date("Y-m-d",strtotime($last));
		    $now_date = date("Y-m-d",strtotime($row['Date']));

		    if($lastdate == $now_date){
		    	$last_time = date("Y-m-d H:i:s",strtotime($row['Date']));
			    $now_time = date("Y-m-d H:i:s",strtotime($last));

			    //compute total time
			    $to_time = strtotime($last_time);
			    $from_time = strtotime($now_time);
			    $totaltime = $to_time - $from_time;

			    if($totaltime <= 15){
			    	if($distance >= .15){
			    		$row_arr = array(
			            'type' => 'Feature',
			            'geometry' => [
			                            'type' =>'LineString',
			                            'coordinates' => [ [$lang , $lat],
			                            	[$lastlang , $lastlat] 
			                        	]
			                          ],
			            'properties' => [
			                                 'color' => '#b30000' // red
			                            ]
			            );

			            array_push($features, $row_arr);
			    	}else{

			    		$row_arr = array(
			            'type' => 'Feature',
			            'geometry' => [
			                            'type' =>'LineString',
			                            'coordinates' => [ [$lang , $lat],
			                            	[$lastlang , $lastlat] 
			                        	]
			                          ],
			            'properties' => [
			                                 'color' => '#000080' // blue
			                            ]
			            );


			            array_push($features, $row_arr);
			    	}
			    }else{
			    	$row_arr = array(
			            'type' => 'Feature',
			            'geometry' => [
			                            'type' =>'LineString',
			                            'coordinates' => [ [$lang , $lat],
			                            	[$lastlang , $lastlat] 
			                        	]
			                          ],
			            'properties' => [
			                                 'color' => '#000080' // blue
			                            ]
			            );


			    		array_push($features, $row_arr);
			    }

			    $coor = [ $lang , $lat ];    
			    //array_push($features, $coor);

			    $lastlat = $lat;
			    $lastlang = $lang;
			    $last = $row['Date'];
		    }


		}else{
			$loc = explode(",", $row['Location']);
		    $lat = floatval($loc[0]);
		    $lang = floatval($loc[1]);

		    $coor = [ $lang , $lat ];    
		    array_push($features, $coor);

		    $lastlat = $lat;
		    $lastlang = $lang;
		    $last = $row['Date'];
		}

		$count = $count + 1;
	}


    $response = array();
    $response['type'] = "FeatureCollection";
    $response['features'] = $features;

    echo json_encode($response);
}

function distance ($lat1, $lon1, $lat2, $lon2, $unit){
	$theta = $lon1-$lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	$distance = $miles * 1.609344;

	return round($distance,2);
}
?>