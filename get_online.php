<?php 
include_once 'connection.php';
session_start();

    $get_dev = mysqli_query($con,"SELECT * from tbl_device where UserID = '$_SESSION[user_id]' ORDER BY TrackerID ASC");
    
    while($row = mysqli_fetch_array($get_dev)){
    $status = mysqli_query($con,"SELECT * from tbl_location where TrackerID = '$row[TrackerID]' ORDER BY Date DESC limit 1");
    $get_status = mysqli_fetch_array($status);
    $lastdate = date("Y-m-d",strtotime($get_status['Date']));
    $now_date = date("Y-m-d");

    $last_time = date("Y-m-d H:i:s",strtotime($get_status['Date']));
    $now_time = date("Y-m-d H:i:s");

    //compute total time
    $to_time = strtotime($now_time);
    $from_time = strtotime($last_time);
    $totaltime = round(abs($to_time - $from_time)/60);

    if($lastdate == $now_date){
        if($totaltime < 5){
            $loc = explode(",", $get_status['Location']);
    ?>
            <tr>
            <td><?php echo $row['Name']; ?></td>
            <td><?php echo $loc[0]."<br>".$loc[1];?></td>
            <td><center><span class="btn btn-danger">Online</span></center></td>
            <td><button value="<?php echo $loc[1].",".$loc[0]; ?>" onclick="relocate(this.value)" class="btn btn-primary btn-small"><span class="fa fa-map-marker"> Locate</span></button>
                <button value="<?php echo $loc[1].",".$loc[0]; ?>" onclick="fence(this.value)" class="btn btn-success btn-small"><span class="fa fa-map-signs"> Geofence</span></button></td>
            </tr>
    <?php
        }else{
    ?>
            <tr>
            <td><?php echo $row['Name']; ?></td>
            <td></td>
            <td><center><span class="btn btn-gray">Offline</span></center></td>
            <td></td>
            </tr>

    <?php
        }

    }else{
    ?>
            <tr>
            <td><?php echo $row['Name']; ?></td>
            <td></td>
            <td><center><span class="btn btn-gray">Offline</span></center></td>
            <td></td>
            </tr>
   <?php
        }
    }
?>