<?php 
include_once 'connection.php';

if(isset($_POST['dev_id'])){
   $id = $_POST['dev_id'];
    $from = $_POST['from'];
    $to = $_POST['to'];

    $a = date("Y-m-d H:i:s", strtotime($from));
    $b = date("Y-m-d H:i:s", strtotime($to));

    $query = mysqli_query($con, "SELECT * FROM `tbl_location` WHERE TrackerID = '$id' AND Date>='$a' AND Date<='$b'");
    //$query = mysqli_query($con, "SELECT * from tbl_location where TrackerID = '$id' AND DATE(Date) = '$from'");
    $total = 0;
    $count = 1;
    $overspeed = 0;
    while($row = mysqli_fetch_array($query)){
		if($count > 1){

		    $loc = explode(",", $row['Location']);
		    $lat = floatval($loc[0]);
		    $lang = floatval($loc[1]);

		    $unit = "K";
		    $distance = distance($lastlat, $lastlang, $lat, $lang, $unit);

		    if($distance >= .15){
		    	$overspeed = $overspeed + 1;
		    }

		    $total = $total + $distance;

		}else{
			$loc = explode(",", $row['Location']);
		    $lat = floatval($loc[0]);
		    $lang = floatval($loc[1]);

		    $lastlat = $lat;
		    $lastlang = $lang;
		    $last = $row['Date'];
		}

		$count = $count + 1;
	}

	$first = mysqli_query($con, "SELECT * FROM `tbl_location` WHERE TrackerID = '$id' AND Date>='$a' AND Date<='$b' ORDER BY Date ASC Limit 1");
	$get_first = mysqli_fetch_array($first);
	$last = mysqli_query($con, "SELECT * FROM `tbl_location` WHERE TrackerID = '$id' AND Date>='$a' AND Date<='$b' ORDER BY Date DESC Limit 1");
	$get_last = mysqli_fetch_array($last);

	if($overspeed == 0){
		$overspeed_status = "NO";
	}else{
		$overspeed_status = "YES";
	}

	/*
	echo "<br>";
	echo "<hr>";
	echo "<br>";
	echo "<h5>Other Information</h5>";
	echo "<table class='table table-bordered'>";
	echo "<tbody>";
	echo "<tr>";
	echo "<td>Start Time:</td>";
	echo "<td>".$get_first['Date']."</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td>End Time:</td>";
	echo "<td>".$get_last['Date']."</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td>Overspeed:</td>";
	echo "<td>".$overspeed_status."</td>";
	echo "</tr>";
	echo "</tbody>";                  
    echo "</table>";
    */
}

function distance ($lat1, $lon1, $lat2, $lon2, $unit){
	$theta = $lon1-$lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	$distance = $miles * 1.609344;

	return round($distance,2);
}
?>