#include "Adafruit_FONA.h" //library

// standard pins for the shield, adjust as necessary
#define FONA_RX 2
#define FONA_TX 3
#define FONA_RST 4

// We default to using software serial. If you want to use hardware serial
// (because softserial isnt supported) comment out the following three lines 
// and uncomment the HardwareSerial line
#include <SoftwareSerial.h>
SoftwareSerial fonaSS = SoftwareSerial(7, 8);
SoftwareSerial *fonaSerial = &fonaSS;

// Hardware serial is also possible!
//  HardwareSerial *fonaSerial = &Serial1;
Adafruit_FONA fona = Adafruit_FONA(FONA_RST);

// Have a FONA 3G? use this object type instead
//Adafruit_FONA_3G fona = Adafruit_FONA_3G(FONA_RST);

//pin for push button
const int buttonPin = 2;     // the number of the pushbutton pin
// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int messageCounter = 0;

void setup() {

  while (! Serial);

  Serial.begin(115200);
  Serial.println(F("Adafruit FONA 808 & 3G GPS demo"));
  Serial.println(F("Initializing FONA... (May take a few seconds)"));

  //check if sim 808 is on
  fonaSerial->begin(4800);
  if (! fona.begin(*fonaSerial)) {
    Serial.println(F("Couldn't find FONA"));
    while(1);
  }
  Serial.println(F("FONA is OK"));
  
  // Try to enable GPRS
  Serial.println(F("Enabling GPS..."));
  fona.enableGPS(true);

  pinMode(buttonPin, INPUT_PULLUP);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  
  delay(2000);

  //fona library get gps
  float latitude, longitude, speed_kph, heading, speed_mph, altitude;

  // if you ask for an altitude reading, getGPS will return false if there isn't a 3D fix
  //Check if gps is now lock-in.
  boolean gps_success = fona.getGPS(&latitude, &longitude, &speed_kph, &heading, &altitude);

  //if locked in
  if (gps_success) {
    //if gps is available open GPRS(Data connection)
    Serial.println(F("Enabling GPRS..."));
    fona.enableGPRS(true);

    Serial.print("GPS lat:");
    Serial.println(latitude, 6);
    Serial.print("GPS long:");
    Serial.println(longitude, 6);
    Serial.print("GPS speed KPH:");
    Serial.println(speed_kph);
    Serial.print("GPS speed MPH:");
    speed_mph = speed_kph * 0.621371192;
    Serial.println(speed_mph);
    Serial.print("GPS heading:");
    Serial.println(heading);
    Serial.print("GPS altitude:");
    Serial.println(altitude);
    
    // Post data to website
    uint16_t statuscode;
    int16_t length;
    char url[80];
    char data[80];

    //DECLARATION OF DATA
    char a[10];
    String lat = dtostrf(latitude,1,5,a); //latitude
    String longi = dtostrf(longitude,1,5,a); //longitude
    String loc = "\""+lat+","+longi+"\"";
    String id = "\"1002\""; //device id e.g. 1001, 1002, 1003

    //DECLARATION OF WEBSITE
    String web = "http://mnvehicletracker.com/gpslocation.php"; //website url save to database
    web.toCharArray(url,80); //URL = website & 80 = Length if converted to charArray
    String dataa = "{\"id\":"+id+",\"data\": "+loc+"}"; //Data to be passed
    Serial.println(dataa);
    dataa.toCharArray(data,80); //data = Data to be pass & 80 = Length if converted to charArray

    //send to website
    fona.HTTP_POST_start(url, F("text/plain"), (uint8_t *) data, strlen(data), &statuscode, (uint16_t *)&length);  

    //turn off gprs
    Serial.println(F("Disabling GPRS..."));
    fona.enableGPRS(false);

    if(buttonState == HIGH){
      Serial.println("SOS - OFF");
      messageCounter = 0;
    }else{
        Serial.println("SOS - ON");
        if(messageCounter == 0){

        //if gps is available open GPRS(Data connection)
        Serial.println(F("Enabling GPRS..."));
        fona.enableGPRS(true);

        char url1[80];
        char data1[80];  
        String username = "\"mntracker\""; //username
        
        //DECLARATION OF WEBSITE
        String web1 = "http://mnvehicletracker.com/gps_sms.php"; //sms
        web1.toCharArray(url1,80); //URL = website & 80 = Length if converted to charArray
        String dataa1 = "{\"id\":"+id+",\"username\": "+username+"}"; //Data to be passed
        Serial.println(dataa1);
        dataa1.toCharArray(data1,80); //data = Data to be pass & 80 = Length if converted to charArray
    
        //send to website
        fona.HTTP_POST_start(url1, F("text/plain"), (uint8_t *) data1, strlen(data1), &statuscode, (uint16_t *)&length);  

        messageCounter = messageCounter + 1;

        
        //turn off gprs
        Serial.println(F("Disabling GPRS..."));
        fona.enableGPRS(false);
      }
    }


  } else {
    Serial.println("Waiting for FONA GPS 3D fix...");
  }
}
